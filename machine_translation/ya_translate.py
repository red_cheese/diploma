import codecs
import httplib
import xml.etree.ElementTree as ET

TRANSLATE_IN = "booking.com.reviews.our.format.xml.en.xml.out"
#TRANSLATE_IN = "test-trans.xml"
TRANSLATE_OUT = "booking.com.reviews.translate.out"
KEY = "trnsl.1.1.20150528T135755Z.3550f95ad8b9eaf2.1f558c52af0d356abdceb55d290f324851d804aa"
CONN = httplib.HTTPSConnection("translate.yandex.net")
URL = "/api/v1.5/tr/translate?"

def trans(w):
    w = w.replace('\n', '.').split('.')
    txt = None
    for wl in w:
        if len(wl) < 2:
            continue
        try:
            CONN.request("GET", URL + "key=" + KEY + "&text=" + wl + "&lang=en-ru")
            r = CONN.getresponse().read()
            tr = ET.fromstring(r)
            #print tr
            #print tr.attrib
            f = tr.find('text')
            if f is not None:
                txt = f.text if txt is None else txt + "." + f.text
        except Exception as e:
            print "ERROR", e
    #print w
    #print txt
    return txt

def ya_translate():
    tree = ET.iterparse(TRANSLATE_IN, events=('end',))
    f_out = codecs.open(TRANSLATE_OUT, 'w', 'utf-8')
    f_out.write("<txts>")
    for event, elem in tree:
        if event == 'end' and elem.tag == 'rv':
            id = None
            txt = None
            for chld in elem:
                if chld.tag == 'id':
                    id = chld.text
                if chld.tag == 'txt':
                    to_trans = chld.text
                    # Yandex translate
                    txt = trans(to_trans)
            if id is not None and txt is not None:
                f_out.write("<rv>")
                f_out.write("<id>" + id + "</id>")
                f_out.write("<txt>" + txt + "</txt>")
                f_out.write("</rv>")
    f_out.write("</txts>")
    f_out.close()

def main():
    ya_translate();

if __name__ == "__main__":
    main()
