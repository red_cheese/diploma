import codecs
import xml.etree.ElementTree as ET

BOOKING_IN = "urls_4_reviews_priorities.tsv"
BOOKING_OUT = "urls_4_reviews_priorities.tsv.out"
XML_IN = "booking.com.reviews.our.format.xml.en.xml"
XML_OUT = "booking.com.reviews.our.format.xml.en.xml.out"

URL_IDS = {};

def booking_ids():
    f = open(BOOKING_IN, 'r')
    f_out = open(BOOKING_OUT, 'w')
    for line in f.read().strip().split('\n'):
        line = line.split('\t')
        booking = line[10]
        if len(booking) > 5:
            f_out.write("%s,%s\n" % (booking, line[0]))

def fill_ids():
    global URL_IDS
    f = open(BOOKING_OUT, 'r')
    for line in f.read().strip().split('\n'):
        line = line.split(',')
        URL_IDS[line[0][:-7] + "html"] = line[1]
            
def parse_xml():
    fill_ids()
    tree = ET.iterparse(XML_IN, events=('end',))
    f_out = codecs.open(XML_OUT, 'w', 'utf-8')
    f_out.write("<txts>")
    h_cnt = 0
    g_cnt = 0
    for event, elem in tree:
        if event == 'end' and elem.tag == 'review':
            h_cnt += 1
            #url = None
            id = None
            txt = ""
            for chld in elem:
                if chld.tag == 'url':
                    url = chld.text.split('?')[0]
                    if url in URL_IDS:
                        id = URL_IDS[url]
                if chld.tag == 'pros' or chld.tag == 'contras':
                    for c in chld:
                        if c.text is not None:
                            txt += c.text;
            #f_out.write("<url>" + url + "</url>")
            if id is not None:
                g_cnt += 1
                f_out.write("<rv>")
                f_out.write("<id>" + id + "</id>")
                f_out.write("<txt>" + txt + "</txt>")
                f_out.write("</rv>")
            print "reviews: %d, matches: %d" % (h_cnt, g_cnt)
    f_out.write("</txts>")
    
def main():
    #booking_ids()
    parse_xml()
    pass
    
if __name__ == "__main__":
    main()
