import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author Alexandra Mikhaylova mikhaylova.alexandra.a@gmail.com
 */
public class TomitaFacts {
    public TomitaFacts() {
        this._positive = new HashMap<>();
        this._negative = new HashMap<>();
    }

    public TomitaFacts(Map<String, Set<String>> positive, Map<String, Set<String>> negative) {
        this._positive = positive;
        this._negative = negative;
    }

    public boolean containsParam(String param) {
        return _positive.containsKey(param) || _negative.containsKey(param);
    }

    public boolean containsParamAndOpinion(String param, String opinion) {
        return (_positive.containsKey(param) && _positive.get(param).contains(opinion))
                || (_negative.containsKey(param) && _negative.get(param).contains(opinion));
    }

    public Map<String, Set<String>> positive() {
        return _positive;
    }

    public Map<String, Set<String>> negative() {
        return _negative;
    }

    private Map<String, Set<String>> _positive;
    private Map<String, Set<String>> _negative;
}
