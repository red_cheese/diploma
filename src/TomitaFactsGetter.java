import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Alexandra Mikhaylova mikhaylova.alexandra.a@gmail.com
 */
public class TomitaFactsGetter {
    public TomitaFacts getTomitaFacts() {
        return new TomitaFacts(getTomitaFacts(true), getTomitaFacts(false));
    }

    private Map<String, Set<String>> getTomitaFacts(boolean positive) {
        Map<String, Set<String>> facts = new HashMap<>();
        String tomita = positive ? _tomitaPositive : _tomitaNegative;

        try {
            BufferedReader in  = new BufferedReader(new FileReader(tomita));
            String str;
            while ((str = in.readLine()) != null) {
                final String[] tokens = str.split(_delimiters);
                for (int i = 1; i < tokens.length; i++) {
                    final String entity = tokens[i];
                    // Временная мера в рамках таски об оценке точности базы по параметру
                    //if (!_entities.contains(entity)) {
                    //    continue;
                    //}
                    if (facts.containsKey(entity)) {
                        facts.get(entity).add(tokens[0]);
                    } else {
                        facts.put(entity, new HashSet<>(Arrays.asList(tokens[0])));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return facts;
    }

    //private String _tomitaNegative = "data/tomita_neg_small";
    //private String _tomitaPositive = "data/tomita_pos_small";
    private String _tomitaNegative = "data/tomita_neg";
    private String _tomitaPositive = "data/tomita_pos";
    private Set<String> _entities = new HashSet<>(Arrays.asList("интернет", "wi-fi"));

    private String _delimiters = "[\t,]+";

    public static void main(String[] args) {
        TomitaFactsGetter tomitaFactsGetter = new TomitaFactsGetter();
        TomitaFacts tomitaFacts = tomitaFactsGetter.getTomitaFacts();
        for (Map.Entry<String, Set<String>> positiveFact : tomitaFacts.positive().entrySet()) {
            System.out.println(positiveFact.getKey() + ": " + positiveFact.getValue().toString());
        }
    }
}