import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Alexandra Mikhaylova mikhaylova.alexandra.a@gmail.com
 */
public class HotelFactsFrequencyAnalyzer extends HotelFactsAnalyzer {
    public HotelFactsFrequencyAnalyzer() {
        init();
        _facts = new HashMap<>();
        _topFacts = new HashMap<>();
        _tomitaFacts = new TomitaFactsGetter().getTomitaFacts();
    }

    public void addFacts(Set<Integer> hotelIds) {
        int i = 0;

        for (int hotelId: hotelIds) {
            for (Map.Entry<HotelFact, Integer> hotelFact : getFactsForHotel(hotelId, false, null).entrySet()) {
                HotelFact fact = hotelFact.getKey();
                int freq = hotelFact.getValue();
                _facts.put(
                        fact,
                        _facts.containsKey(fact) ? _facts.get(fact) + freq : freq
                );
            }

            System.out.println("Hotels ready: " + ++i);
        }
    }

    public void addTopFacts() {
        System.out.println("Calculating top facts...");

        List<HotelFact> factsList = new ArrayList<>(_facts.keySet());
        Collections.sort(factsList, new Utils.FactComparator<>(_facts));
        for (int i = 0; i < factsList.size() * Utils.HOTEL_FACTS_FREQUENCY_THRESHOLD; i++) {
            HotelFact hotelFact = factsList.get(i);
            _topFacts.put(hotelFact, _facts.get(hotelFact));

            System.out.println("Added top facts: " + i);
        }

        System.out.println("Done calculating top facts");
    }

    @Override
    public void train() {
        System.out.println("Start training...");
        System.out.println("Calculating fact stats...");

        addFacts(Utils.readHotelIds(Utils.RESORT_IDS_ALL));
        //addTopFacts();
        _topFacts = _facts;

        System.out.println("Done calculating fact stats");
        System.out.println("Serializing fact stats...");

        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(Utils.RESORT_FACTS_STATS_FREQUENCY));

            int i = 0;

            for (Map.Entry<HotelFact, Integer> hEntry : _topFacts.entrySet()) {
                oos.writeObject(hEntry.getKey());
                oos.writeInt(hEntry.getValue());

                System.out.println("Top facts serialized: " + ++i);
            }
            oos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Done");
    }

    @Override
    public void test() {
        // Deserialize top facts
        _topFacts.clear();
        _facts.clear();
        System.out.println("De-serializing top facts...");
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(Utils.RESORT_FACTS_STATS_FREQUENCY));
            while (true) {
                HotelFact k = (HotelFact) ois.readObject();
                int v = ois.readInt();
                _topFacts.put(k, v);
            }
        } catch (EOFException e) {
            // skip
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Done de-serializing top facts");


        /** TODO Put here the name of the file with ID list */
        Set<Integer> hIds = Utils.readHotelIds(Utils.RESORT_IDS_ALL);
        //Set<Integer> hIds = Utils.getRandomHotelIds(Utils.RESORT_IDS_N_NUMBER);
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(Utils.RESORT_FACTS_STATS_FREQUENCY_TEST));
            int resCnt = 0;
            for (int hId : hIds) {
                Map<HotelFact, Integer> facts = getFactsForHotel(hId, true, null);
                int cnt = 0;
                for (Map.Entry<HotelFact, Integer> hotelFact : facts.entrySet()) {
                    cnt += hotelFact.getValue();
                }
                if (cnt == 0) { cnt = 1; }
                for (Map.Entry<HotelFact, Integer> hotelFact : facts.entrySet()) {
                    HotelFact fact = hotelFact.getKey();
                    HotelFact factNoSaveNegative = new HotelFact(fact.id(), fact.param(), fact.opinion(), fact.isNegative(), false);
                    double rateHotel = ((double) hotelFact.getValue()) / cnt;
                    double rateDB = ((double) _topFacts.get(factNoSaveNegative)) / Utils.RESORT_FACTS;
                    double rate = rateHotel / rateDB;
                    if (hotelFact.getValue() > 1 && rate > 1.0) {
                        out.write(hId + "\t" + factNoSaveNegative.toString() + "\t" + fact.isNegative() + "\t" + rateHotel + "\t" + rateDB);
                        out.newLine();
                    }
                }
                System.out.println("Resorts ready: " + ++resCnt);
            }
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        HotelFactsFrequencyAnalyzer hotelFactsFrequencyAnalyzer = new HotelFactsFrequencyAnalyzer();

        //Utils.writeHotelIds(hotelFactsFrequencyAnalyzer.getAllHotelIds(), Utils.RESORT_IDS_ALL);

        //hotelFactsFrequencyAnalyzer.train();
        hotelFactsFrequencyAnalyzer.test();
    }

    private Map<HotelFact, Integer> _facts;
    private Map<HotelFact, Integer> _topFacts;

    private TomitaFacts _tomitaFacts;
}
