import java.io.Serializable;

/**
 * @author Alexandra Mikhaylova mikhaylova.alexandra.a@gmail.com
 */
public class HotelFact implements Comparable<HotelFact>, Serializable {
    public HotelFact(int id, String param, String opinion, boolean isNegative, boolean checkIsNegative) {
        _id = id;
        _param = param;
        _opinion = opinion;
        _checkIsNegative = checkIsNegative;
        _isNegative = isNegative;
    }

    public int id() {
        return _id;
    }

    public String param() {
        return _param;
    }

    public String opinion() {
        return _opinion;
    }

    public boolean checkIsNegative() {
        return _checkIsNegative;
    }

    public boolean isNegative() {
        return _isNegative;
    }

    public String toString() {
        String s = _param + ": " + _opinion;
        return _checkIsNegative
                ? s + " " + _isNegative
                : s;
    }

    @Override
    public int compareTo(HotelFact o) {
        return _checkIsNegative
                ? Integer.compare(_id, o._id)
                : toString().compareTo(o.toString());
    }

    @Override
    public boolean equals(Object o) {
        HotelFact ho = (HotelFact) o;
        return _checkIsNegative
                ? _id == ho._id
                : _param.equals(ho._param) && _opinion.equals(ho._opinion);
    }

    @Override
    public int hashCode() {
        return _checkIsNegative
                ? Integer.valueOf(_id).hashCode()
                : toString().hashCode();
    }

    private int _id;
    private String _param;
    private String _opinion;
    private boolean _checkIsNegative;
    private boolean _isNegative;
}
