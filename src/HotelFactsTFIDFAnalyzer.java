import java.io.*;
import java.sql.*;
import java.util.*;

/**
 * @author Alexandra Mikhaylova mikhaylova.alexandra.a@gmail.com
 */
public class HotelFactsTFIDFAnalyzer {
    public HotelFactsTFIDFAnalyzer() {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            _connection = DriverManager.getConnection(
                    HotelFactsFrequencyAnalyzer.DB_ADDR,
                    HotelFactsFrequencyAnalyzer.DB_USR,
                    HotelFactsFrequencyAnalyzer.DB_PWD
            );
            _reviewIdSt = _connection.prepareStatement(
                    "SELECT review_id FROM hotels_reviews WHERE hotel_id = ?"
            );
            _factIdSt = _connection.prepareStatement(
                    "SELECT fact_id FROM reviews_facts WHERE review_id = ?"
            );
            _factSt = _connection.prepareStatement(
                    "SELECT * FROM facts WHERE id = ?"
            );
            _hotelCountSt = _connection.prepareStatement(
                    "SELECT COUNT(DISTINCT hotel_id) FROM facts JOIN reviews_facts ON facts.id = reviews_facts.fact_id JOIN hotels_reviews ON reviews_facts.review_id = hotels_reviews.review_id WHERE facts.id = ?"
            );
            ResultSet rs = _connection.createStatement().executeQuery("SELECT COUNT(DISTINCT hotel_id) FROM hotels_reviews");
            if (rs.next()) {
                _allHotelsNumber = rs.getInt("COUNT(DISTINCT hotel_id)");
            }
        } catch (Exception e) {
            _connection = null;
            e.printStackTrace();
        }
    }

    public Map<String, Double> tfidfForHotel(int hId) {
        Map<String, Double> res = new HashMap<>();
        Map<HotelFact, Integer> factToFrequency = new HashMap<>();
        int factCount = 0;
        if (_connection != null) {
            try {
                _reviewIdSt.setInt(1, hId);
                ResultSet reviewIdRs = _reviewIdSt.executeQuery();
                while (reviewIdRs.next()) {
                    _factIdSt.setInt(1, reviewIdRs.getInt("review_id"));
                    ResultSet factIdRs = _factIdSt.executeQuery();
                    while (factIdRs.next()) {
                        _factSt.setInt(1, factIdRs.getInt("fact_id"));
                        ResultSet factRs = _factSt.executeQuery();
                        while (factRs.next()) {
                            int id = factRs.getInt("id");
                            String param = factRs.getString("param").toLowerCase();
                            String opinion = factRs.getString("opinion").toLowerCase();
                            boolean isNegative = factRs.getBoolean("is_negative");
                            HotelFact fact = new HotelFact(id, param, opinion, isNegative, false);
                            factToFrequency.put(fact, factToFrequency.containsKey(fact) ? factToFrequency.get(fact) + 1 : 1);
                            factCount++;
                        }
                    }
                }

                for (Map.Entry<HotelFact, Integer> ff : factToFrequency.entrySet()) {
                    double tf = factToFrequency.get(ff.getKey()) * 1.0 / factCount;
                    _hotelCountSt.setInt(1, ff.getKey().id());
                    ResultSet hotelCountRs = _hotelCountSt.executeQuery();
                    int hotelCount = 0;
                    if (hotelCountRs.next()) {
                        hotelCount = hotelCountRs.getInt(1);
                    }
                    double idf = Math.log(_allHotelsNumber * 1.0 / hotelCount);
                    res.put(ff.getKey().toString(), tf * idf);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return res;
    }

    public static void main(String[] args) {
        HotelFactsTFIDFAnalyzer analyzer = new HotelFactsTFIDFAnalyzer();
        Set<Integer> hIds = Utils.readHotelIds(Utils.HOTEL_IDS_N);

        System.out.println("Calculating TF*IDF...");
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(Utils.HOTEL_FACTS_TFIDF_N_NO_DICT));
            int i = 0;
            for (int hId : hIds) {
                System.out.println("Doing for hotel #" + ++i + " ID: " + hId);
                out.newLine();
                out.write("===============");
                out.newLine();
                out.write("ID: " + hId);
                out.newLine();
                Map<String, Double> m = analyzer.tfidfForHotel(hId);
                String[] fs = m.keySet().toArray(new String[m.keySet().size()]);
                Arrays.sort(fs, new Utils.FactComparator(m)); // TODO: fix all this
                for (String f : fs) {
                    out.write(f + " / " + m.get(f));
                    out.newLine();
                }
                out.write("===============");
                out.newLine();
                System.out.println("Done for hotel #" + i + " ID: " + hId);
            }
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Connection _connection;

    private PreparedStatement _reviewIdSt;
    private PreparedStatement _factIdSt;
    private PreparedStatement _factSt;
    private PreparedStatement _hotelCountSt;

    private int _allHotelsNumber;
}
