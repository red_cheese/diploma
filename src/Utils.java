import java.io.*;
import java.util.*;

/**
 * @author Alexandra Mikhaylova mikhaylova.alexandra.a@gmail.com
 */
public class Utils {
    public static int TEST_N = 1;

    public static String HOTEL_IDS_ALL = "data/hotel_ids_all";
    public static String HOTEL_IDS_N = "data/hotel_ids_n";
    /**
     * HOTEL_IDS_ALL_NUMBER should be changed each time the DB is changed.
     * */
    public static int HOTEL_IDS_ALL_NUMBER = 2998;
    public static int HOTEL_IDS_N_NUMBER = 100;

    public static double HOTEL_FACTS_FREQUENCY_THRESHOLD = 0.35;
    // Временная мера
    public static String HOTEL_FACTS_STATS_FREQUENCY = "data_results_frequency/hotel_facts_stats_INTERNET.ser";
    public static String HOTEL_FACTS_STATS_FREQUENCY_TEST = "data_results_frequency/hotel_facts_stats_test_INTERNET";

    public static String HOTEL_FACTS_TFIDF_N_NO_DICT = "data_results_tfidf/hotel_facts_tfidf_n_no_dict";

    public static String RESORT_IDS_ALL = "data/resort_ids_all";
    public static String RESORT_IDS_N = "data/resort_ids_n";
    public static int RESORT_IDS_ALL_NUMBER = 1047;
    public static int RESORT_IDS_N_NUMBER = 300;
    public static String RESORT_FACTS_STATS_FREQUENCY = "data_results_frequency/resort_facts_stats.ser";
    public static String RESORT_FACTS_STATS_FREQUENCY_TEST = "data_results_frequency/resort_facts_stats_test";

    public static int RESORT_FACTS = 1334626; // Number of serialized resort facts

    public static Set<Integer> getRandomHotelIds(int n) {
        Set<Integer> res = new TreeSet<>();

        Set<Integer> all = readHotelIds(HOTEL_IDS_ALL);
        Set<Integer> resN = new TreeSet<>();
        Random rand = new Random();

        int i = 0;
        while (i < n) {
            int j = rand.nextInt(all.size());
            if (resN.contains(j)) continue;
            resN.add(j);
            i++;
        }

        i = 0;
        for (int hId : all) {
            if (resN.contains(i)) {
                res.add(hId);
            }
            i++;
        }

        return res;
    }

    public static Set<Integer> readHotelIds(String filename) {
        Set<Integer> res = new TreeSet<>();

        try {
            BufferedReader in = new BufferedReader(new FileReader(filename));
            String str;
            while ((str = in.readLine()) != null) {
                res.add(Integer.parseInt(str));
            }
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return res;
    }

    public static void writeHotelIds(Set<Integer> hIds, String filename) {
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(filename));
            for (int hId : hIds) {
                out.write(Integer.toString(hId));
                out.newLine();
            }
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static class FactComparator<T extends Comparable<? super T>> implements Comparator<HotelFact> {
        public FactComparator(Map<HotelFact, T> map) {
            _map = map;
        }

        @Override
        public int compare(HotelFact o1, HotelFact o2) {
            return - _map.get(o1).compareTo(_map.get(o2));
        }

        private Map<HotelFact, T> _map;
    }
}
