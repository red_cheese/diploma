import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author Alexandra Mikhaylova mikhaylova.alexandra.a@gmail.com
 */
public abstract class HotelFactsAnalyzer {
    public static String DB_ADDR = "jdbc:mysql://localhost/resorts";
    public static String DB_USR = "root";
    public static String DB_PWD = "";

    /* IDs of all hotels that have reviews in our database. */
    public Set<Integer> getAllHotelIds() {
        Set<Integer> res = new TreeSet<>();
        if (_connection != null) {
            try {
                Statement st = _connection.createStatement();
                ResultSet rs = st.executeQuery(
                        "SELECT DISTINCT hotel_id FROM hotels_reviews"
                );
                while (rs.next()) {
                    res.add(rs.getInt("hotel_id"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return res;
    }

    public Map<HotelFact, Integer> getFactsForHotel(int hotelId, boolean saveIsNegative, TomitaFacts tomitaFacts) {
        Map<HotelFact, Integer> facts = new HashMap<>();

        //int hotelFactCount = 0;

        if (_connection != null) {
            try {
                _reviewIdSt.setInt(1, hotelId);
                ResultSet reviewIdRs = _reviewIdSt.executeQuery();
                while (reviewIdRs.next()) {
                    _factIdSt.setInt(1, reviewIdRs.getInt("review_id"));
                    ResultSet factIdRs = _factIdSt.executeQuery();
                    while (factIdRs.next()) {
                        _factSt.setInt(1, factIdRs.getInt("fact_id"));
                        ResultSet factRs = _factSt.executeQuery();
                        while (factRs.next()) {
                            int id = factRs.getInt("id");
                            String param = factRs.getString("param").toLowerCase();
                            String opinion = factRs.getString("opinion").toLowerCase();
                            if (tomitaFacts != null) {
                                if (!tomitaFacts.containsParamAndOpinion(param, opinion)) {
                                    continue;
                                }
                            }
                            boolean isNegative = factRs.getBoolean("is_negative");
                            HotelFact hotelFact = new HotelFact(id, param, opinion, isNegative, saveIsNegative);
                            facts.put(hotelFact, facts.containsKey(hotelFact) ? facts.get(hotelFact) + 1 : 1);

                            //System.out.println("Fact: " + ++hotelFactCount);
                        }
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return facts;
    }

    public abstract void train();
    public abstract void test();

    protected void init() {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            _connection = DriverManager.getConnection(DB_ADDR, DB_USR, DB_PWD);
            _reviewIdSt = _connection.prepareStatement(
                    "SELECT review_id FROM hotels_reviews WHERE hotel_id = ?"
            );
            _factIdSt = _connection.prepareStatement(
                    "SELECT fact_id FROM reviews_facts WHERE review_id = ?"
            );
            _factSt = _connection.prepareStatement(
                    "SELECT * FROM facts WHERE id = ?"
            );
        } catch (Exception e) {
            _connection = null;
            e.printStackTrace();
        }
    }

    protected Connection _connection;
    protected PreparedStatement _reviewIdSt;
    protected PreparedStatement _factIdSt;
    protected PreparedStatement _factSt;
}
